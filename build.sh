pip install -r requirements.txt
pushd front_app
yarn install
yarn build
popd
python monster_se.py > jobs/__fixture.json
python stepstone_se.py > jobs/__fixture2.json
pushd jobs
ln -s ../front_app/front feed/static/feed
python manage.py makemigrations
python manage.py makemigrations feed
python manage.py migrate
python manage.py loaddata __fixture.json
python manage.py loaddata __fixture2.json
python manage.py runserver
