var gulp = require('gulp');
var uglify = require('gulp-uglify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');

gulp.task('default', function() {
  var b = browserify({
    entries: './src/main.js',
    debug: true
  });
    return b.bundle().pipe(source("bundle.js")).pipe(buffer())
    .pipe(gulp.dest('./build/'));
});
gulp.task('compress', function() {
  return gulp.src('build/bundle.js')
    .pipe(uglify()).on('error', function(err) { console.log(err); })
    .pipe(gulp.dest('build/min'));
});

gulp.task('collect',function(){
	gulp.src(['css/**',
		'images/**',
		'templates/**'],{base:'.'})
	.pipe(gulp.dest('front'));
	gulp.src(
		'build/min/bundle.js')
	.pipe(gulp.dest('front/js'));

		});
/*


gulp.task('compress', function() {
  return gulp.src('lib/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});
*/
