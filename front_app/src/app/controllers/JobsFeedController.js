var JobsFeedController =  function($scope,$http,myModal, idService) {
  $scope.items = [];
  $scope.template = {url:"/static/feed/templates/main.html"};
  $scope.showModal = myModal.activate;
  $scope.getItems = function() {
        $http({method : 'GET',url : '/location/all/'}).then(
          function(data, status) {
            $scope.items = data.data;
        },
        function(data, status) {
            console.log("Error");
        }
        );

  };

  $scope.activateForm = function(id){
    $scope.showModal();
    idService.addId(id);
  }

};

exports.JobsFeedController = JobsFeedController;
