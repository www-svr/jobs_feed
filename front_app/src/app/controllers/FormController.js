var FormController = function (myModal ,$scope , $http, idService) {
	this.closeMe = myModal.deactivate;

	  $scope.loadForm = function(){
			console.log(idService.getId());
	    var url = "/apply/%s/".replace("%s", idService.getId()[0]);
	    $http({method : 'GET',url : url}).then(
	      function(data, status) {
	        $scope.applicationForm = data.data;
	        console.log(  $scope.applicationForm);

	    },
	    function(data, status) {
	        console.log("Error");
	    }
	    );
	  };
	  $scope.applicationForm = {};
	  $scope.submitForm = function(){

	    $http({
				method: 'POST',
				url: "/apply/%s/".replace("%s", $scope.applicationForm.position),
				data: angular.toJson($scope.applicationForm),
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function (resp) {

				myModal.deactivate();
			},
			function (resp) {
				console.log("dead");
			}

			);
	  };

};

exports.FormController = FormController;
