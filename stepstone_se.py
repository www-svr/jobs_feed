import requests
import lxml.html
from lxml.html.clean import clean_html


def fetch_page(url):
    response = requests.get(url)
    return response.text

def load_xml_tree(tree):
    root = lxml.html.document_fromstring(tree)
    root.make_links_absolute('https://www.stepstone.se/',resolve_base_href=False)
    jobs = root.cssselect('#list-style-foongus')[0]
    clean_jobs = clean_html(lxml.html.tostring(jobs))
    return clean_jobs

def map_distinct_job(job_tree):
    job = {}
    job['title']=job_tree.cssselect(".description>h5>a")[0].text
    job['company']=job_tree.cssselect(".description>span.text-bold>a")[0].text
    job['location']=job_tree.cssselect(".description>span.subtitle>span")[1].text
    return job

def parse_distinct_jobs(jobs_tree):
    root = lxml.html.fragment_fromstring(jobs_tree)
    distinct_jobs = root.cssselect('article')
    return list(map(lambda x: map_distinct_job(x),distinct_jobs) )


if __name__ == '__main__':
    url = "http://www.stepstone.se/lediga-jobb-i-hela-sverige/data-it/"
    resp = fetch_page(url)
    job_tree = load_xml_tree(resp)
    distinct_jobs = parse_distinct_jobs(job_tree)
    fixtures = list(map(lambda x: {"model": "feed.job", "fields":x}, distinct_jobs))
    print(json.dumps(fixtures))
