from rest_framework import serializers
from django.contrib.auth.models import User
from feed.models import Job, Application, Question, QuestionAnswer, QuestionResult


class JobsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = '__all__'

class QuestionAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionAnswer
        fields = ("text", "id")

class QuestionSerializer(serializers.ModelSerializer):
    related_question = QuestionAnswerSerializer(read_only=True,many=True)
    class Meta:
        model = Question
        fields = ('text', "mode", "related_question", 'id')

class QuestionResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionResult
        fields = ("question","answer","application")


class ApplicationSerializer(serializers.ModelSerializer):
    question = QuestionSerializer(read_only=True,many=True)
    answers = QuestionResultSerializer(many=True)
    class Meta:
        model = Application
        fields = ('email', "name", "address", "phone" ,"question", "position", "answers")
        # extra_kwargs = {"position":{'write_only': True,'default':''}}
