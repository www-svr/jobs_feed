from django.conf.urls import url
from feed import views

urlpatterns = [
    url(r'location/(?P<location>[^\\]+)/$', views.list_jobs, name='feed'),
    url(r'apply/(?P<position_id>\d+)/$', views.make_application, name='apply'),
    url(r'.*$', views.index, name='index'),
]
