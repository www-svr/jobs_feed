from django.contrib import admin

from feed.models import Job, Application, Question, QuestionAnswer, QuestionResult
# Register your models here.
admin.site.register(Job)
admin.site.register(Application)
admin.site.register(Question)
admin.site.register(QuestionAnswer)
admin.site.register(QuestionResult)
