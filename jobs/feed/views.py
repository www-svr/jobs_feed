from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from rest_framework.renderers import JSONRenderer

from feed.serializers import JobsSerializer, ApplicationSerializer, QuestionSerializer
from feed.models import Job, Application, Question
import json

# Create your views here.
def index(request):
    return render(request,'feed/index.html')

def list_jobs(request, location='all'):
    query = []
    if location == 'all':
        query = Job.objects.all()
    else:
        query = Job.objects.filter(location=location).all()
    serializer = JobsSerializer(query, many=True)
    resp = HttpResponse(content_type = 'application/json')
    resp.content = JSONRenderer().render(serializer.data)
    return resp

def make_application(request,position_id):
    if request.method == "GET":
        query = Question.objects.all()
        serial_questions = QuestionSerializer(query,many=True)
        data={"question" :serial_questions.data,"position":position_id,
                'email':None,'name':None,'phone':None,"address":None }

        resp = HttpResponse(content_type = 'application/json')
        resp.content = JSONRenderer().render(data)
        return resp
    if request.method == "POST":
        js = str(request.body.decode('utf-8'))
        serializer = ApplicationSerializer(data=json.loads(js))
        if serializer.is_valid():
            serializer.save()
            resp = HttpResponse(content_type = 'application/json')
            resp.content = JSONRenderer().render({'Status':'OK'})
            return resp
        else:
            resp = HttpResponse(content_type = 'application/json')
            resp.status_code=400
            resp.content = JSONRenderer().render(serializer.errors)
            return resp
