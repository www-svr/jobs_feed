from django.db import models

# Create your models here.
class Job(models.Model):
    """Available job positions"""
    title = models.CharField(max_length=125)
    company = models.CharField(max_length=125)
    location = models.CharField(max_length=125)

class Application(models.Model):
    """Users application"""
    email = models.EmailField()
    position = models.ForeignKey(Job,related_name='position_applied')
    name = models.CharField(max_length=125)
    phone = models.CharField(max_length=125,blank=True,null=True)
    address = models.CharField(max_length=125,blank=True,null=True)
    class Meta:
        unique_together = ("email", "position")

class Question(models.Model):
    text = models.CharField(max_length=255)
    mode = models.IntegerField()
    def __str__(self):
        return "{}".format(self.text)

class QuestionAnswer(models.Model):
    text = models.CharField(max_length=125)
    question = models.ForeignKey(Question,related_name='related_question')
    custom_answer = models.BooleanField()
    def __str__(self):
        return "{}".format(self.text)

class QuestionResult(models.Model):
    application = models.ForeignKey(Application,related_name='related_application')
    question = models.ForeignKey(Question,related_name='application_question')
    answer = models.ForeignKey(Application,related_name='application_answer')
