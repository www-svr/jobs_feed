import requests
import lxml.html
from lxml.html.clean import clean_html
import json

def fetch_page(url):
    response = requests.get(url)
    return response.text

def load_xml_tree(tree):
    root = lxml.html.document_fromstring(tree)
    root.make_links_absolute('https://www.monster.se/',resolve_base_href=False)
    jobs = root.cssselect('#resultsWrapper')[0]
    clean_jobs = clean_html(lxml.html.tostring(jobs))
    return clean_jobs

def map_distinct_job(job_tree):
    job = {}
    job['title']=job_tree.cssselect(".jobTitle>h2>a>span")[0].text
    job['company']=job_tree.cssselect(".company>a>span")[0].text
    try:
        job['location']=job_tree.cssselect(".location>a>span")[0].text
    except Exception as e:
        job['location']=job_tree.cssselect(".location>span")[0].text
    job["location"]=" ".join(job["location"].split(r'\n\r')).strip()
    return job

def parse_distinct_jobs(jobs_tree):
    root = lxml.html.fragment_fromstring(jobs_tree)
    distinct_jobs = root.cssselect('.js_job-bold>article.js_result_row')
    return list(map(lambda x: map_distinct_job(x),distinct_jobs) )


if __name__ == '__main__':
    url = "https://www.monster.se/jobb/sok/Data-IT_4?intcid=swoop_BrowseJobs_Data-IT"
    resp = fetch_page(url)
    job_tree = load_xml_tree(resp)
    distinct_jobs = parse_distinct_jobs(job_tree)
    fixtures = list(map(lambda x: {"model": "feed.job", "fields":x}, distinct_jobs))
    print(json.dumps(fixtures))
